var express = require("express");
var router = express.Router();

// /* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index', { title: 'Express' });
// });

router.get("/", function(req, res, next) {
  res.render("index", { pageId: "home", title: "Home | Food Website" });
});

router.get("/breakfast", function(req, res, next) {
  res.render("breakfast", {
    pageId: "breakfast",
    title: "Breakfast | Food Website"
  });
});

router.get("/lunch", function(req, res, next) {
  res.render("lunch", {
    pageId: "lunch",
    title: "Lunch | Food Website"
  });
});

module.exports = router;
