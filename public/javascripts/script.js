$(document).ready(function () {
    var element = $('meta[name="active-menu"]').attr('content');
    $('#' + element).addClass('active');
    console.log(moment());

    // initalizing Carousel
    $('.carousel').carousel({
        interval: 5000,
        pause: true,
        touch: true
    })



    $(window).bind('mousewheel', function (event) {
        var $win = $(window);
        if (event.originalEvent.wheelDelta >= 0) {
            $('#navbar').addClass('bg-white');
            $('#navbar a,#navbar span').addClass('text-dark').removeClass('text-white');
            $('#navbar #navContainer').removeClass('navContainer');
            console.log('Scroll up');
            if ($win.scrollTop() == 0) {
                // console.log('Top');
                // $('#navbar').removeClass('bg-white');
                // $('#navbar a,#navbar span').removeClass('text-dark').addClass('text-white');
                // $('#navbar #navContainer').addClass('navContainer');
               
            }
        }
        else {
            console.log('Scroll down');
            $('#navbar').addClass(' ');
            $('#navbar a,#navbar span').addClass('text-dark').removeClass('text-white');
            $('#navbar #navContainer').removeClass('navContainer');
            
        }
    });
    $('#navbar a').mouseenter(function () {
        $(this).addClass('hover');
        console.log('Enter');
    });

    $('#navbar a').mouseleave(function () {
        $(this).removeClass('hover');
    });

});